
//class is _not fully supported_
class Tamagotchi {
//constructs the object
    constructor(name, type, energy, selector){
        this.name = name;
        this.type = type;
        this.energy = energy;
        this.element = document.querySelector(selector);


        //start the simulation
        setInterval(this.update.bind(this), 1000);
    }
//exposes debug data
    report() {
        console.log(this.name + " is a " + this.type + " and has E: " + this.energy);
    }

    //add energy
    eat() {
        //add one energy
        if (this.energy != 0) {
            this.energy += 10;
        }
    }

    //runs the tamagotchi 'sim'
    update() {
        if(this.energy == 0) {
            this.element.innerHTML = this.name + " has ran away";

        }else {
            this.energy--;
            this.element.innerHTML = this.name + " has " + this.energy + " energy";
        }
    }

}

var myTamagotchi = new Tamagotchi("Guy", "heart", 5, "#dvInfo");
myTamagotchi.report(); //expose debug data

