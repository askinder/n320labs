Vue.component('dice-roller',{
   template: "<div><div>{{ currentRoll }}</div><button v-on:click='rollDie'>Roll Dice</button></div>",
    data: function(){
       return {
           currentRoll: 0
       }
    },
    methods: {
        rollDie: function(){
            this.currentRoll = 1+Math.floor(Math.random() * 6);
        }

    }

});

//selects the app div for vue
new Vue({
    el: '#app'
});